import common.ConfigurationBuilder;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class TestRunner {
    @Parameters({"platformType", "applicationName"})
    @BeforeSuite
    public void init(String platformType, String applicationName) {
        System.out.println();
        System.out.println("===============================================");
        System.out.println(platformType);
        System.out.println(applicationName);
        System.out.println("===============================================");
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setPlatform(platformType)
                .setTestedApp(applicationName)
                .build();
    }
}
