package apiTests;

import api.ApiManager;
import api.TestHelper;
import api.descriptors.Article;
import api.descriptors.Response;
import api.entities.SectionsEnum;
import api.entities.TimePeriod;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

public class CheckResponseAPITest extends APITestRunner {
    private ApiManager apiManager;

    @BeforeClass
    public void initialize() {
        apiManager = new ApiManager();
        apiManager
                .setSection(SectionsEnum.ALL_SECTIONS)
                .setTimePeriod(TimePeriod.ONE_DAY);
        Response response = apiManager.sendRequest();

        HashMap<String, Integer> articlesAmount = TestHelper.getArticlesAmount(response);
        CheckResponseDataProvider.generateDataProvider(articlesAmount);
    }

    @Test(dataProvider = "CheckResponseTestData", dataProviderClass = CheckResponseDataProvider.class)
    public void getAllArticlesTest(String section, int expectedAmount) {
        Response responseToCheck = apiManager.setSection(section).sendRequest();

        for (Article article : responseToCheck.results) {
            Assert.assertEquals(article.section, section);
        }

        Assert.assertEquals(responseToCheck.results.size(), expectedAmount);
    }
}
