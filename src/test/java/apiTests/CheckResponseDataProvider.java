package apiTests;

import org.testng.annotations.DataProvider;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class CheckResponseDataProvider {
    private static Object[][] articlesArray;

    public static void generateDataProvider(HashMap<String, Integer> articlesAmount) {
        // Array size of 2 because of key value arguments
        articlesArray = new Object[articlesAmount.size()][2];
        Set entries = articlesAmount.entrySet();
        Iterator entriesIterator = entries.iterator();

        int i = 0;
        while (entriesIterator.hasNext()) {
            Map.Entry mapping = (Map.Entry) entriesIterator.next();

            articlesArray[i][0] = mapping.getKey().toString();
            articlesArray[i][1] = mapping.getValue();

            i++;
        }
        System.out.println("CheckResponseTestData - data provider is generated");
    }

    @DataProvider(name = "CheckResponseTestData")
    public Object[][] getData() {
        return articlesArray;
    }
}
