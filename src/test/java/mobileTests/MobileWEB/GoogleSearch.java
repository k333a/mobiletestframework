package mobileTests.MobileWEB;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import mobileTests.MobileTestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.ArrayList;

public class GoogleSearch extends MobileTestBase {

    @Test
    public void GoogleSerchTest() throws Exception{
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus 6");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");

        System.out.println("Step 1. Create new driver");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        WebDriverWait wait = new WebDriverWait(driver, 30);

        System.out.println("Step 2. Open website");
        driver.get("https://google.com");

        System.out.println("Step 3. Enter search string");
        driver.findElementByName("q").sendKeys("Appium");

        System.out.println("Step 4. Press Search button");
        driver.findElementByClassName("Tg7LZd").click();

        System.out.println("Step 5. Check result");
        Boolean isPresent = driver.findElements(By.id("main")).size() > 0;

        Assert.assertTrue(isPresent, "Result should be present.");

        System.out.println("Step 7. Close driver");
        driver.quit();
    }
}
