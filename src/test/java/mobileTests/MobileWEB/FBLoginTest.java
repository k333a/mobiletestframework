package mobileTests.MobileWEB;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import mobileTests.MobileTestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.net.URL;

import static org.testng.Assert.assertTrue;

public class FBLoginTest extends MobileTestBase {

    @Test
    public void DummyTest() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus 6");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");

        System.out.println("Step 1. Create new driver");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        WebDriverWait wait = new WebDriverWait(driver, 30);

        System.out.println("Step 2. Open website");
        driver.get("https://facebook.com");

        System.out.println("Step 3. Enter email");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
        driver.findElement(By.name("email")).sendKeys("max@test.com");

        System.out.println("Step 4. Enter password");
        driver.findElement(By.name("pass")).sendKeys("123456");

        System.out.println("Step 5. Click Login button");
        driver.findElement(By.name("login")).click();

        System.out.println("Step 6. Check error message");
        Thread.sleep(5000);
        assertTrue(driver.findElement(By.cssSelector("div[data-sigil='m_login_notice']")).getText().contains("The password you entered is incorrect"));

        System.out.println("Step 7. Close driver");
        driver.quit();
    }
}
