package mobileTests;

import io.appium.java_client.android.AndroidDriver;
import mobile.appium.AppiumServerManager;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class MobileTestBase {
    protected static AndroidDriver driver;
    protected static AppiumServerManager appiumServer;

    @BeforeSuite
    protected void Initialize() {
        appiumServer = new AppiumServerManager();
        appiumServer.startServer();
    }

    @AfterSuite
    protected void Dispose() {
        appiumServer.stopServer();
    }
}
