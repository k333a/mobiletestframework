package mobileTests.MobileNative.Calculator;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import mobile.PageElements.Calculator.CalculatorPage;
import mobileTests.MobileTestBase;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class StartApplication extends MobileTestBase {
    @BeforeMethod
    public void startCalculator() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
        capabilities.setCapability("appPackage", "com.android.calculator2");
        capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void closeApp() {
        driver.quit();
    }

    @Test
    public void checkButtonsFunctionality() throws Exception {
        double valueToEnter = 0.123456789;
        CalculatorPage page = new CalculatorPage(driver);
        page.getKeyboard().enterValue(valueToEnter);
        Thread.sleep(10000);
        Assert.assertEquals(page.getDisplay().getFormulaText(), String.valueOf(valueToEnter));
    }

    @Test
    public void enterValue() throws Exception {
        CalculatorPage page = new CalculatorPage(driver);
        page.getKeyboard().enterValue(100).multiplyBy(2).decreaseBy(4).increseBy(4);
        Thread.sleep(10000);
        Assert.assertEquals(page.getDisplay().getFormulaText(), "100x2-4+4");
        page.getKeyboard().pressEqual();
    }
}