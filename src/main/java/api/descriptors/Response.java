package api.descriptors;

import java.util.ArrayList;

public class Response {
    public String status;
    public String copyright;
    public int num_results;
    public ArrayList<Article> results;
}

