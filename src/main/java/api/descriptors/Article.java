package api.descriptors;

import java.util.Date;

public class Article {
    public String url;
    public String count_type;
    public String column;
    public String section;
    public String byline;
    public String title;
    public String Abstract;
    public Date published_date;
    public String source;
    public Object des_facet;
    public Object per_facet;
    public Object geo_facet;
    public Object media;
}