package api;

import api.descriptors.Article;
import api.descriptors.Response;

import java.util.HashMap;

public class TestHelper {
    public static HashMap<String, Integer> getArticlesAmount(Response response) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();

        for (Article article : response.results) {
            if (map.get(article.section) == null) {
                map.put(article.section, 1);
            } else {
                map.put(article.section, map.get(article.section) + 1);
            }
        }
        return map;
    }
}
