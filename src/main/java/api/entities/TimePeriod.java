package api.entities;

public enum TimePeriod {
    ONE_DAY("1"),
    WEEK("7"),
    MONTH("30");

    private String value;

    TimePeriod(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
