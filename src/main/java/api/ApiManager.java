package api;

import api.descriptors.Response;
import api.entities.SectionsEnum;
import api.entities.TimePeriod;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ApiManager {
    private static final String API_PATH = "https://api.nytimes.com/svc/mostpopular/v2/mostemailed";
    private static final String API_KEY = "6d090a705a5a4343860775bc71febd7d";
    private static final String KEY_PARAM = "?api-key=";
    private static final String OFFSET_PARAM = "&offset=";
    private static final int ARTICLES_PER_PAGE = 20;
    private static String section = "";
    private static String timePeriod = "";
    private static String params = "";
    private final Gson gson;


    public ApiManager() {
        gson = new Gson();
    }

    public ApiManager setSection(SectionsEnum parameters) {
        setSection(parameters.getValue());
        return this;
    }

    public ApiManager setSection(String parameters) {
        section = buildSectionPath(parameters);
        return this;
    }

    public ApiManager setTimePeriod(TimePeriod parameters) {
        timePeriod = parameters.getValue() + ".json";
        return this;
    }

    public ApiManager setParams(String parameters) {
        params = parameters;
        return this;
    }

    public Response sendRequest() {
        Response responseObject = getResponse();
        int pagesAmount = responseObject.num_results / ARTICLES_PER_PAGE;

        for (int i = 1; i <= pagesAmount; i++) {
            setParams(OFFSET_PARAM + i * ARTICLES_PER_PAGE);
            Response page = getResponse();
            responseObject.results.addAll(page.results);
        }

        setParams("");
        return responseObject;
    }

    private String buildSectionPath(String section) {
        return section.replaceAll(" ", "%20").replaceAll("/", "%2F");
    }

    private String buildTotalPath() {
        StringBuilder totalPath = new StringBuilder();
        totalPath
                .append(API_PATH)
                .append("/")
                .append(section)
                .append("/")
                .append(timePeriod)
                .append(KEY_PARAM)
                .append(API_KEY)
                .append(params);
        return totalPath.toString();
    }

    private Response getResponse() {
        Response responseObject = null;
        try {
            Client client = Client.create();
            WebResource webResource = client
                    .resource(buildTotalPath());

            ClientResponse response = webResource.accept("application/json")
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            String responseString = response.getEntity(String.class);
            System.out.println(responseString);
            responseObject = gson.fromJson(responseString, Response.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseObject;
    }
}
