package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverLauncher {
    WebDriverType webDriverType;
    DesiredCapabilities capabilities;

    public WebDriverLauncher(WebDriverType webDriverType) {
        this.webDriverType = webDriverType;
    }

    public WebDriver getDriver() {
        switch (webDriverType) {
            case IE:
                return new InternetExplorerDriver();
            case CHROME:
                return new ChromeDriver();
            case FIREFOX:
                return new FirefoxDriver();
            default:
                throw new IllegalArgumentException("Requested driver type is not found");
        }
    }

    private void setWebDriverCapabilities() {
        capabilities = new DesiredCapabilities();
    }

}
