package common;

public enum WebDriverType {
    CHROME,
    FIREFOX,
    IE
}
