package mobile.appium;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.util.Properties;

public class AppiumServerManager {
    private static final String APPIUM_CONFIG_PATH = "src/main/java/mobile/configuration/AppiumServerConfig.properties";
    private static final String APPIUM_HOST_PROPERTY_NAME = "AppiumHost";
    private static final String APPIUM_PORT_PROPERTY_NAME = "AppiumPort";
    private AppiumDriverLocalService service;
    private AppiumServiceBuilder builder;
    private DesiredCapabilities cap;
    private int AppiumPort = 0;
    private String AppiumHost = "";


    public AppiumServerManager() {
        LoadProperties();
    }

    public void startServer() {
        if (IsServerRunning()) {
            return;
        }

        cap = new DesiredCapabilities();
        cap.setCapability("noReset", "false");

        //Build the Appium service
        builder = new AppiumServiceBuilder();
        builder.withIPAddress(AppiumHost);
        builder.usingPort(AppiumPort);
        builder.withCapabilities(cap);
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");

        //Start the server with the builder
        service = AppiumDriverLocalService.buildService(builder);
        service.start();

        System.out.println("Appium server started");
    }

    public void stopServer() {
        service.stop();
        System.out.println("Appium server stopped");
    }

    private void LoadProperties() {
        System.out.println("Loading AppiumServer settings...");
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(APPIUM_CONFIG_PATH);
            prop.load(input);

            // get the property value
            AppiumPort = Integer.parseInt(prop.getProperty(APPIUM_PORT_PROPERTY_NAME));
            System.out.println("Loaded Appium Port, it is: " + AppiumPort);
            AppiumHost = prop.getProperty(APPIUM_HOST_PROPERTY_NAME);
            System.out.println("Loaded Appium Host, it is: " + AppiumHost);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private boolean IsServerRunning() {
        boolean isServerRunning = false;
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(AppiumPort);
            serverSocket.close();
        } catch (IOException e) {
            //If control comes here, then it means that the port is in use
            isServerRunning = true;
            System.out.println("Appium server is already running...");
        } finally {
            serverSocket = null;
        }
        return isServerRunning;
    }
}
