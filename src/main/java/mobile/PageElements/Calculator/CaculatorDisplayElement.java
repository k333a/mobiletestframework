package mobile.PageElements.Calculator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CaculatorDisplayElement extends CalculatorPage {
    @FindBy(id = "com.android.calculator2:id/result")
    private WebElement result_text;

    @FindBy(id = "com.android.calculator2:id/formula")
    private WebElement entered_text;

    public String getFormulaText() {
        return entered_text.getText();
    }

    public String getResult() {
        return result_text.getText();
    }
}
