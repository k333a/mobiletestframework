package mobile.PageElements.Calculator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CalculatorKeyboardElement extends CalculatorPage {
    @FindBy(id = "com.android.calculator2:id/digit_0")
    private WebElement btn_0;
    @FindBy(id = "com.android.calculator2:id/digit_1")
    private WebElement btn_1;
    @FindBy(id = "com.android.calculator2:id/digit_2")
    private WebElement btn_2;
    @FindBy(id = "com.android.calculator2:id/digit_3")
    private WebElement btn_3;
    @FindBy(id = "com.android.calculator2:id/digit_4")
    private WebElement btn_4;
    @FindBy(id = "com.android.calculator2:id/digit_5")
    private WebElement btn_5;
    @FindBy(id = "com.android.calculator2:id/digit_6")
    private WebElement btn_6;
    @FindBy(id = "com.android.calculator2:id/digit_7")
    private WebElement btn_7;
    @FindBy(id = "com.android.calculator2:id/digit_8")
    private WebElement btn_8;
    @FindBy(id = "com.android.calculator2:id/digit_9")
    private WebElement btn_9;
    @FindBy(id = "com.android.calculator2:id/dec_point")
    private WebElement btn_dot;
    @FindBy(id = "com.android.calculator2:id/eq")
    private WebElement btn_equal;
    @FindBy(id = "com.android.calculator2:id/del")
    private WebElement btn_backspace;
    @FindBy(id = "com.android.calculator2:id/clr")
    private WebElement btn_clear;
    @FindBy(id = "com.android.calculator2:id/op_div")
    private WebElement btn_divide;
    @FindBy(id = "com.android.calculator2:id/op_mul")
    private WebElement btn_multiply;
    @FindBy(id = "com.android.calculator2:id/op_sub")
    private WebElement btn_minus;
    @FindBy(id = "com.android.calculator2:id/op_add")
    private WebElement btn_plus;

    public CalculatorKeyboardElement enterValue(int value) {
        enterValue(String.valueOf(value));
        return this;
    }

    public CalculatorKeyboardElement enterValue(double value) {
        enterValue(String.valueOf(value));
        return this;
    }

    public CalculatorKeyboardElement multiplyBy(int value) {
        btn_multiply.click();
        enterValue(String.valueOf(value));
        return this;
    }

    public CalculatorKeyboardElement divideBy(int value) {
        btn_divide.click();
        enterValue(String.valueOf(value));
        return this;
    }

    public CalculatorKeyboardElement decreaseBy(int value) {
        btn_plus.click();
        enterValue(String.valueOf(value));
        return this;
    }

    public CalculatorKeyboardElement increseBy(int value) {
        btn_minus.click();
        enterValue(String.valueOf(value));
        return this;
    }

    public CalculatorKeyboardElement pressEqual() {
        btn_equal.click();
        return this;
    }

    //    TODO Need to check functionality of this method
    public CalculatorKeyboardElement clear() {
        if (btn_backspace != null) {
            while (getDisplay().getFormulaText() != "") {
                btn_backspace.click();
            }
        } else {
            btn_clear.click();
        }

        return this;
    }

    private void enterValue(String value) {
        System.out.println("Value to enter: " + value);
        char[] digits = value.toCharArray();

        for (char digit : digits) {
            pressButton(digit);
        }
    }

    private void pressButton(char digit) {
        switch (digit) {
            case '0':
                btn_0.click();
                break;
            case '1':
                btn_1.click();
                break;
            case '2':
                btn_2.click();
                break;
            case '3':
                btn_3.click();
                break;
            case '4':
                btn_4.click();
                break;
            case '5':
                btn_5.click();
                break;
            case '6':
                btn_6.click();
                break;
            case '7':
                btn_7.click();
                break;
            case '8':
                btn_8.click();
                break;
            case '9':
                btn_9.click();
                break;
            case '.':
                btn_dot.click();
                break;
            default:
                throw new IllegalArgumentException("Check the entered value. Requested digit: " + digit + "is not found on keyboard");
        }
        System.out.println(digit + " is pressed");
    }

}
