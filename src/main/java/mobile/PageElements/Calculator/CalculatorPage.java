package mobile.PageElements.Calculator;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.support.PageFactory;

public class CalculatorPage {
    protected AppiumDriver driver;
    private CalculatorKeyboardElement keyboard;
    private CaculatorDisplayElement display;

    public CalculatorPage(AppiumDriver driver) {
        this.driver = driver;
    }

    public CalculatorPage() {
    }

    public CalculatorKeyboardElement getKeyboard() {
        if (keyboard == null) {
            keyboard = new CalculatorKeyboardElement();
            PageFactory.initElements(driver, keyboard);
        }
        return keyboard;
    }

    public CaculatorDisplayElement getDisplay() {
        if (display == null) {
            display = new CaculatorDisplayElement();
            PageFactory.initElements(driver, display);
        }
        return display;
    }
}
